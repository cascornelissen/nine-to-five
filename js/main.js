var json, time = [];
var settings = {
    exactMode: true,
    language: 'en',
    theme: 'strawberry',
}

$(document).ready(function() {
    loadSettings();
    
    // Settings: Exact Mode
    $('.toggle-exactmode').on('click', function() {
        toggleExactMode();
    });
    
    // Settings: Languages
    $('.language span').on('click', function() {
        setLanguage($(this).attr('data-val'));
    });
    
    // Settings: Theme
    $('.theme span').on('click', function() {
        setTheme($(this).attr('data-val'));
    });
});

$(window).load(function() {
	$('body').removeClass('loading');
    updateBigText();
});

$(window).resize(function() {
    updateBigText();
});

$(window).mousemove(function() {
    $('#settings').addClass('show');
    if ( typeof settingsTimeout !== 'undefined' ) clearTimeout(settingsTimeout);
    settingsTimeout = setTimeout(function() {
        $('#settings').removeClass('show');
    }, 1500);
});

function updateClock(force) {
    force = typeof force !== 'undefined' ? force : false;
    
    var now = new Date();
    var hours_current     = (now.getHours() > 12) ? now.getHours()-12: now.getHours();
    var hours_next        = (hours_current == 12) ? 0 : hours_current+1;
    var minutes_current   = now.getMinutes();
    var minutes_left      = 60-minutes_current;
    var minutes_left_half = Math.abs(30-minutes_current);
    
    if ( !settings.exactMode ) {
        minutes_current = (round5(minutes_current) >= 60) ? 0 : round5(minutes_current);
        minutes_left = (round5(minutes_left) >= 60) ? 0 : round5(minutes_left);
        minutes_left_half = (round5(minutes_left_half) >= 60) ? 0 : round5(minutes_left_half);
    }
    
    var text = time[minutes_current];
    
    // Replacing parameters
    text = text.replace('{hours_current}'     , '<strong>' + json.numbers[hours_current] + '</strong>');
    text = text.replace('{hours_next}'        , '<strong>' + json.numbers[hours_next] + '</strong>');
    text = text.replace('{minutes_current}'   , '<strong>' + json.numbers[minutes_current] + '</strong>');
    text = text.replace('{minutes_left}'      , '<strong>' + json.numbers[minutes_left] + '</strong>');
    text = text.replace('{minutes_left_half}' , '<strong>' + json.numbers[minutes_left_half] + '</strong>');
    text = '<span>' + text + '</span>';
    
    // Set time
    var t = hours_current + ':' + minutes_current;
    $('#time').removeClass('animate')
    
    if ( force ) {
        $('#time').html(text).attr('data-time', t);
        updateBigText();
    } else if ( t != $('#time').attr('data-time') ) {
        $('#time').addClass('animate');
        setTimeout(function() {
            $('#time').html(text).attr('data-time', t);
            updateBigText();
        }, 500);
    }
}

function loadTranslation() {
    $.getJSON('lang/' + settings.language + '.json', function(data) {
        json = data;
        createTimeArray(json.strings.time);
        
        // Initialize clock
        updateClock(true);
        setInterval(updateClock, 1000);
    });
}

function toggleExactMode() {
    settings.exactMode = !settings.exactMode;
    var val = (settings.exactMode) ? 'ON' : 'OFF';
    $('.toggle-exactmode .value').text(val);
    updateClock(true);
    
    saveSettings();
}

function setTheme(theme) {
    settings.theme = theme;
    $('#theme').attr('href', 'css/themes/' + theme + '.css');
    $('.theme .active').removeClass('active');
    $('.theme span[data-val="' + theme + '"]').parent().addClass('active');
    
    saveSettings();
}

function setLanguage(language) {
    settings.language = language;
    $('.language .active').removeClass('active');
    $('.language span[data-val="' + language + '"]').parent().addClass('active');
    
    loadTranslation();
    saveSettings();
}

function saveSettings() {
    localStorage.setItem('settings', JSON.stringify(settings));
}

function loadSettings() {
	var s = jQuery.parseJSON(localStorage.getItem('settings'));
    settings = (s !== null) ? s : settings;
    
    // Theme & Language
    setTheme(settings.theme);
    setLanguage(settings.language);
    loadTranslation();
    
    // Exact mode
    var val = (settings.exactMode) ? 'ON' : 'OFF';
    $('.toggle-exactmode .value').text(val);
}

function updateBigText() {
    var el = ($(window).width() > $(window).height()) ? $('#time') : $('#time > span') ;
    el.bigtext();
}

function createTimeArray(json) {
    $.each(json, function(i, item) {
        $.each(item, function(i, value) {
            if ( i.indexOf('-') > -1 ) {
                var boundaries = i.split('-');
                for ( i = boundaries[0]; i <= boundaries[1]; i++ ) {
                    time[i] = value;
                }
            } else {
                time[i] = value;    
            }
        })
    });
}

function round5(x) {
    return (x % 5) >= 2.5 ? parseInt(x / 5) * 5 + 5 : parseInt(x / 5) * 5;
}