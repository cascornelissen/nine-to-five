# Nine to Five #
*Modern multilingual clock that utilizes big typography*

## New themes and languages ##
Feel free to create a pull request if you want to submit a new theme or language (or fixes for existings themes and languages). Themes are located in `/css/themes/` and languages can found in `/lang/`.