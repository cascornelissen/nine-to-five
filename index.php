<!DOCTYPE html>
<html lang="en">
<head>
	<title>Nine to Five</title>
	<link href='//fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/themes/default.css" id="theme">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="js/bigtext.js"></script>
	<script src="js/main.js"></script>
	<meta name="robots" content="noindex, nofollow">
</head>
<body class="loading">
    <noscript>
        <div class="small notification">
            <h2>Enable JavaScript</h2>
            <p>It looks like you've disabled JavaScript, but Nine to Five really needs it to run!</p>
        </div>
    </noscript>
    
    <div class="vertical-center-container">
        <div class="vertical-center">
            <div id="time-container">
                <h1 id="time">
                    <span><strong>Nine</strong> <span>To</span> <strong>Five</strong></span>
                </h1>
            </div>
        </div>
    </div>
    
    <ul id="settings">
        <li><span class="toggle-exactmode">Exact mode: <span class="value">ON</span></span></li>
        <li class="dropdown">
            <span>Theme</span>
            <ul class="theme">
                <li class="active"><span data-val="strawberry">Strawberry</span></li>
                <li><span data-val="apple">Apple</span></li>
                <li><span data-val="ocean">Ocean</span></li>
                <li><span data-val="waves">Waves</span></li>
                <li><span data-val="rainy">Rainy</span></li>
                <li><span data-val="grayscale">Grayscale</span></li>
                <li><span data-val="invert">Invert</span></li>
            </ul>
        </li>
        <li class="dropdown">
            <span>Language</span>
            <ul class="language">
                <li class="active"><span data-val="en">English</span></li>
                <li><span data-val="nl">Dutch</span></li>
                <li><span data-val="de">German</span></li>
            </ul>
        </li>
    </ul>
</body>
</html>